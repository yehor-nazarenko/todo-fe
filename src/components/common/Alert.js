import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const Alert = (props) => {
    const {
        vertical = 'top',
        horizontal = 'center',
        message = 'Server error',
        type = 'info',
        duration = 2000
    } = props;
    return (
        <Snackbar
            autoHideDuration={duration}
            anchorOrigin={{vertical, horizontal}}
            key={`${vertical},${horizontal}`}
            {...props}
        >
            <MuiAlert
                elevation={6}
                variant="filled"
                severity = {type}
                {...props}
            >
                {message}
            </MuiAlert>
        </Snackbar>
    );
}

export default Alert
