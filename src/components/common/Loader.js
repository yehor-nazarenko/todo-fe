import React from 'react';
import ReactLoading from "react-loading";
import styled from "styled-components";
import PropTypes from "prop-types";


const Loader = (props) => {
    return (
        <LoaderWrapper {...props}>
            <ReactLoading
                type={props.type || "bubbles"}
                color={props.color || '#919191'}
                height={props.height || '10%'}
                width={props.width || '10%'}
            />
        </LoaderWrapper>
    );
};

export default Loader;


export const LoaderWrapper = styled.div`
    display: ${props => props.display};
    justify-content: center;
    height: ${props => props.height};
    width: ${props => props.width};
    position: ${props => props.position};
    left: ${props => props.left};
    top: ${props => props.top};
`;

LoaderWrapper.propTypes = {
    display: PropTypes.string,
    height: PropTypes.string,
    width: PropTypes.string,
    position: PropTypes.string,
    left: PropTypes.string,
    top: PropTypes.string,
};

LoaderWrapper.defaultProps = {
    display: 'block',
    height: '100%',
    width: '100%',
    left: '0',
    top: '0',
};
