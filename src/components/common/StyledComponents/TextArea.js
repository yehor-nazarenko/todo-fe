import styled from "styled-components";
import PropTypes from "prop-types";
import {TextareaAutosize} from "@material-ui/core";

export const TextArea = styled(TextareaAutosize)` 
  display: block;
  margin: auto;
  padding: 0.1rem;
  width: 100%;
  font-size: ${props => props.fontSize};
  font-weight: ${props => props.fontWeight};
  opacity: ${props => props.opacity};
  color: #535353; 
  outline: none;
  resize: none;
  overflow:auto;
  white-space: pre-wrap;
  text-decoration: ${props => props.textDecoration};
  background: ${props => props.background};
  border: ${props => props.border};
  ::placeholder{
    color: #878787; 
  }
 }
 `;

TextArea.propTypes = {
    background: PropTypes.string,
    border: PropTypes.string,
    throughLine: PropTypes.string,
    opacity: PropTypes.string,
    color: PropTypes.string,
    fontWeight: PropTypes.string,
    fontSize: PropTypes.string
};

TextArea.defaultProps = {
    background: '#fff',
    border: 'none',
    textDecoration: 'none',
    opacity: '1',
    fontWeight: 'normal',
    fontSize: '0.9rem'
};
