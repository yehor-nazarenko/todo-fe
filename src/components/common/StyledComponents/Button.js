import styled from "styled-components";
import PropTypes from "prop-types";

export const Button = styled.button`   
  visibility: ${props => props.visibility};
  display: ${props => props.display};
  min-width: 1.5rem;
  min-height: 1.5rem;
  border: none;  
  font-size: 0.9rem;
  color: ${props => props.color};
  background: ${props => props.background};
  &:hover{  
  	cursor: pointer;
  	border-radius: 5px;
  	color: ${props => props.colorHovered};
  	background: #e7e7e7;
  }
`;

Button.propTypes = {
	background: PropTypes.string,
	visibility: PropTypes.string
};

Button.defaultProps = {
	background: '#ffffff',
	visibility: 'visible',
	display: 'inline-block',
	color: '#535353',
	colorHovered: 'rgba(32,33,36,0.8)',
};
