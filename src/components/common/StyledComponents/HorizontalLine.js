import styled from "styled-components";
import PropTypes from "prop-types";

export const HR = styled.hr`   
  visibility: ${props => props.visibility};
  padding: 0;
  margin: 0;
  color: #919191;
`

HR.propTypes = {
    background: PropTypes.string,
    visibility: PropTypes.string
};

HR.defaultProps = {
    background: '#fff',
    visibility: 'visible'
};