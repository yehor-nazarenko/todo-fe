import styled from "styled-components";
import PropTypes from 'prop-types';


export const Card = styled.div` 
  margin: auto;
  padding: 0.2rem 0.6rem 0.2rem 0.6rem;
  background: ${props => props.background};
  box-shadow: 0 2px 10px 0 rgba(0,0,0,0.25);
  border-radius: 0.4rem; 
  :hover{
      box-shadow: 0 2px 10px 4px rgba(0,0,0,0.25);
  }
  font-size: 20px;
`;

Card.propTypes = {
    background: PropTypes.string
};

Card.defaultProps = {
    background: '#fff'
};
