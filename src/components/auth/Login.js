import React, {Fragment, useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import {NavLink} from "react-router-dom";
import {useHistory, useLocation} from "react-router";
import {ErrorMessage, Form, Formik, useField} from "formik";
import {
    Grid,
    Button,
    Paper,
    Checkbox as CheckBoxMui,
    TextField as TextFieldMui
} from "@material-ui/core";
import * as Yup from 'yup';
import Alert from "../common/Alert";
import {useAuth} from "../../store/auth/auth-selector";
import {login} from "../../store/auth/auth-actions";
import {AuthorizationManager} from "../../utils/authorization-manager";
import FormControlLabel from "@material-ui/core/FormControlLabel";


const TextField = ({component: Component, name, ...props}) => {
    const [field, meta] = useField({name, ...props});
    const errorText = meta.error && meta.touched ? meta.error : '';
    return (
        <Fragment>
            <Grid item xs>
                <TextFieldMui
                    style={{minWidth: 250}}
                    {...field}
                    {...props}
                    error={!!errorText}
                />
            </Grid>
            <ErrorMessage name={name}>{msg => msg}</ErrorMessage>
        </Fragment>
    )
};

const CheckBoxField = ({label, ...props}) => {
    const [field] = useField(props);
    return (
        <FormControlLabel
            {...field}
            control={<CheckBoxMui color="default"/>}
            label={label}
        />
    )
}

const initValues = {
    email: '',
    password: '',
    remember_me: false
};

const loginSchema = Yup.object().shape({
    email: Yup.string()
        .required(),
    password: Yup.string()
        .required('No password provided.')
});

const Login = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const location = useLocation();

    const {user, error} = useAuth();
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        if(user) {
            const {from} = location.state || {from: {pathname: "/todos"}};
            history.replace(from);
        }
    }, [user, history, location.state])

    return (
        <Paper>
            <Formik
                initialValues={initValues}
                onSubmit={(values) => {
                     dispatch(login(values))
                        .then((response) => {
                            AuthorizationManager.setToken(response.payload.data.access_token);
                            AuthorizationManager.setUser(values.rememberMe ? user.payload.data : null);
                        })
                        .catch(() => setShowModal(true));
                }}
                validationSchema={loginSchema}
            >{({dirty, isValid}) => (
                <Form>
                    <Grid
                        container
                        direction="column"
                        alignItems="center"
                        spacing={1}
                    >
                        <Grid item>
                            Login
                        </Grid>
                        <TextField
                            placeholder="email"
                            variant="outlined"
                            name="email"
                            type="input"
                        />
                        <TextField
                            placeholder="password"
                            variant="outlined"
                            name="password"
                            type="password"
                        />
                        <CheckBoxField
                            type="checkbox"
                            name="remember_me"
                            label="Remember Me"
                        />
                        <Grid item>
                            <Button
                                disabled={!dirty || !isValid}
                                type="submit"
                            >
                                Submit
                            </Button>
                        </Grid>
                        <Grid
                            item
                            container
                            justify="center"
                            spacing={2}
                        >
                            <Grid item>
                                <NavLink to="/login">
                                    Forgot password?
                                </NavLink>
                            </Grid>
                            <Grid item>
                                <NavLink to="/register">
                                    Don't have an account? Sign Up
                                </NavLink>
                            </Grid>
                        </Grid>
                    </Grid>
                </Form>

            )}
            </Formik>
            <Alert
                duration={3000}
                type='error'
                message={error?.response?.data?.error || error?.message}
                open={showModal}
                onClose={() => setShowModal(false)}
            />
        </Paper>
    );
}

export default Login;
