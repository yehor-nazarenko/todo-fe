import React, {useState} from 'react';
import {
	AppBar,
	Toolbar,
	Typography,
	Button,
	Menu,
	MenuItem,
	Avatar
} from '@material-ui/core';
import {makeStyles} from "@material-ui/core/styles";

import {StyledLink} from "./common/StyledComponents/StyledNavLink";
import {useDispatch} from "react-redux";
import {logout} from "../store/auth/auth-actions";
import {useAuth} from "../store/auth/auth-selector";
import {AuthorizationManager} from "../utils/authorization-manager";


const useStyles = makeStyles(theme => ({
	root: {
		flexGrow: 1,
	},
	spacing: {
		marginRight: theme.spacing(1),
		marginLeft: theme.spacing(1),
	},
	title: {
		flexGrow: 1,
	}
}));

const Header = () => {

	const dispatch = useDispatch();
	const classes = useStyles();

	const {user} = useAuth();
	const [anchorEl, setAnchorEl] = useState(null);


	const handleClickMenu = (event) => {
		setAnchorEl(null);
		if (event.currentTarget.textContent.toLowerCase() === 'logout') {
			dispatch(logout())
				.then(() => {
					AuthorizationManager.clear();
					window.location.reload();
				});
		}
	};

	return (
		<AppBar
			position="static"
			color="default"
		>
			<Toolbar>
				<Typography
					variant="h6"
					className={classes.title}
				>
					<StyledLink to="/">
						Home
					</StyledLink>
				</Typography>
				{user
					? <>

						<Button  size="small" onClick={(e) => setAnchorEl(e.currentTarget)}>
							{user?.username}
							<Avatar src={!!user?.avatar ? user?.avatar : ''} alt='avatar' className={classes.spacing}/>
						</Button>
						<Menu
							id="simple-menu"
							anchorEl={anchorEl}
							keepMounted
							open={!!anchorEl}
							onClose={handleClickMenu}
						>
							<MenuItem onClick={handleClickMenu}>
								<StyledLink to="/profile">My account</StyledLink>
							</MenuItem>
							<MenuItem onClick={handleClickMenu}>Logout</MenuItem>
						</Menu>
					</>
					: <>
						<StyledLink to="/login">
							<Button color="inherit">Sign in</Button>
						</StyledLink>
						<StyledLink to="/register">
							<Button color="inherit">Sign up</Button>
						</StyledLink>
					</>
				}
			</Toolbar>
		</AppBar>
	);
};


export default Header;
