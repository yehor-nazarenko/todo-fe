import React from 'react';
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import {makeStyles} from "@material-ui/core/styles";
import CardActionArea from "@material-ui/core/CardActionArea";
import {Button} from "./common/StyledComponents/Button";
import Grid from "@material-ui/core/Grid";
import {useAuth} from "../store/auth/auth-selector";


const useStyles = makeStyles({
    root: {
        maxWidth: 1200,
    },
    media: {
        height: 140,
    },
});

const Profile = () => {

    const classes = useStyles();
    const {user: {username, avatar_full: avatarFull, email, created_at, updated_at}} = useAuth();

    return (
        <Grid
            item
            container
            justify="center"
            alignItems="stretch"
        >
            <Card className={classes.root}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={avatarFull ? avatarFull : 'https://idinahren'}
                        title="avatar"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {username}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2">
                            {email}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h4">
                            {created_at}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h4">
                            {updated_at}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button color="primary">
                        Edit
                    </Button>
                </CardActions>
            </Card>
        </Grid>
    )
};

export default Profile;
