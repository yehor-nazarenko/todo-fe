import React from 'react';
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";


import {CreateNote} from "./CreateNote";
import {Notes} from "./Notes";


const useStyles = makeStyles(theme => ({
    item: {
        maxWidth: '1280px',
        width: '100%'
    }
}));


const Todos = () => {

    const classes = useStyles();

    return (
        <Grid
            item
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={3}
        >
            <Grid item className={classes.item}>
                <CreateNote/>
            </Grid>
            <Grid item className={classes.item}>
                <Notes/>
            </Grid>
        </Grid>
    );
};

export default Todos;
