import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Checkbox from "@material-ui/core/Checkbox";
import DragIndicator from "@material-ui/icons/DragIndicator";

import {deleteSubNote, editSubNote} from "../../store/todo/todo-actions";
import {TextArea} from "../common/StyledComponents/TextArea";
import {Button} from "../common/StyledComponents/Button";
import {HR} from "../common/StyledComponents/HorizontalLine";
import Box from "@material-ui/core/Box";


export const SubNote = (props) => {

	const dispatch = useDispatch();

	const {noteId, subNote: {id: subNoteId, content, completed}} = props;

	const [inputText, setInputText] = useState(content);
	const [prevStateText, setPrevStateText] = useState(content);
	const [isCompleted, setSubNotStatus] = useState(!!completed);
	const [showElements, setElementsVisibility] = useState(false);
	const [isReadOnlyTextArea, setReadOnlyTextArea] = useState(true);


	const clearInputOrDispatchChanges = (e) => {
		if (e.key === 'Escape') {
			e.preventDefault();
			setInputText('');
		}
		if (!e.shiftKey && e.key === 'Enter') {
			e.preventDefault();
			if (prevStateText !== inputText) {
				dispatch(editSubNote(noteId, subNoteId, inputText, e.target.checked));
				setReadOnlyTextArea(true)
				setPrevStateText(inputText);
			}
		}
	};

	const changeSubNoteStatus = (e) => {
		setSubNotStatus(e.target.checked);
		dispatch(editSubNote(noteId, subNoteId, inputText, e.target.checked));
	};

	const dispatchChanges = () => {
		if (prevStateText !== inputText) {
			dispatch(editSubNote(noteId, subNoteId, inputText, isCompleted))
			setReadOnlyTextArea(true)
			setPrevStateText(inputText);
		}
	}


	return (
		<Box mr={3}>
			<HR visibility={showElements ? 'visible' : 'hidden'}/>
			<Grid
				container
				alignItems="center"
				onMouseEnter={() => setElementsVisibility(true)}
				onMouseLeave={() => setElementsVisibility(false)}
			>
				<Grid item>
					<DragIndicator
						visibility={showElements ? 'visible' : 'hidden'}
						color='disabled'
						viewBox='0 -3 24 24'
						style={{cursor: 'pointer'}}
					/>
				</Grid>
				<Grid item>
					<Checkbox
						size="small"
						color="default"
						value="small"
						checked={isCompleted}
						onChange={changeSubNoteStatus}
						inputProps={{'aria-label': 'checkbox with default color'}}
					/>
				</Grid>
				<Grid item xs>
					<TextArea
						placeholder="list item"
						textDecoration={isCompleted ? 'line-through' : 'none'}
						opacity={isCompleted ? '0.5' : '1'}
						readOnly={isReadOnlyTextArea}
						onMouseDown={(e) => e.detail > 1 ? e.preventDefault() : null}
						onDoubleClick={() => setReadOnlyTextArea(false)}
						value={inputText}
						onKeyDown={clearInputOrDispatchChanges}
						onChange={(e) => setInputText(e.currentTarget.value)}
						onBlur={dispatchChanges}
					/>
				</Grid>
				<Grid item>
					<Button
						visibility={showElements ? 'visible' : 'hidden'}
						onClick={() => dispatch(deleteSubNote(noteId, subNoteId))}
					>
						&#10007;
					</Button>
				</Grid>
			</Grid>
			<HR visibility={showElements ? 'visible' : 'hidden'}/>
		</Box>
	)
};
