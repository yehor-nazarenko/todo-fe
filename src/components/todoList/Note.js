import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

import {deleteNote, editNote} from "../../store/todo/todo-actions";
import {TextArea} from "../common/StyledComponents/TextArea";
import {Card} from "../common/StyledComponents/Card";
import {Button} from "../common/StyledComponents/Button";
import SubNotes from "./SubNotes";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import CreateSubNote from "./CreateSubNote";


const Note = (props) => {

	const dispatch = useDispatch();

	const {note: {id, content = '', children: subNotes = []}} = props;

	const [inputText, setInputText] = useState(content);
	const [prevStateText, setPrevStateText] = useState(content);
	const [showDeleteButton, setDeleteButtonVisible] = useState(false);
	const [showModal, setModalState] = useState(false);
	const [isReadOnlyTextArea, setReadOnlyTextArea] = useState(true);


	const clearInputOrDispatchChanges = (e) => {
		if (e.key === 'Escape') {
			e.preventDefault();
			setInputText('');
		}
		if (!e.shiftKey && e.key === 'Enter') {
			e.preventDefault();
			if (prevStateText !== inputText) {
				dispatch(editNote(id, inputText));
				setReadOnlyTextArea(true)
				setPrevStateText(inputText);
			}
		}
	};

	const dispatchChanges = () => {
		if (prevStateText !== inputText) {
			dispatch(editNote(id, inputText));
			setReadOnlyTextArea(true)
			setPrevStateText(inputText);
		}
	}

	return (
		<Box
			mx={5} mb={2} mt={1}
			onMouseEnter={() => setDeleteButtonVisible(true)}
			onMouseLeave={() => setDeleteButtonVisible(false)}
		>
			<Card>
				<Grid
					container
					direction="column"
					alignItems="center"
					justify="center"
					spacing={1}
				>
					<Grid
						item
						container
						justify="space-between"
					>
						<Grid item xs>
							<TextArea
								fontSize='18px'
								placeholder="Title"
								readOnly={isReadOnlyTextArea}
								onMouseDown={(e) => e.detail > 1 ? e.preventDefault() : null}
								onDoubleClick={() => setReadOnlyTextArea(false)}
								value={inputText}
								onKeyDown={clearInputOrDispatchChanges}
								onChange={(e) => setInputText(e.currentTarget.value)}
								onBlur={dispatchChanges}
							/>
						</Grid>
						<Grid item>
							<Button
								color={'rgba(180,0,0,0.35)'}
								colorHovered={'rgba(180,0,0,0.9)'}
								visibility={showDeleteButton ? 'visible' : 'hidden'}
								onClick={() => setModalState(true)}
							>
								&#10007;
							</Button>
							<Dialog
								open={showModal}
								onClose={() => setModalState(false)}
							>
								<DialogTitle>"Do you really want to do this?"</DialogTitle>
								<DialogActions>
									<Button onClick={() => setModalState(false)} color="primary">
										Cancel
									</Button>
									<Button
										onClick={() => {
											setModalState(false);
											dispatch(deleteNote(id))
										}}
										color="primary"
										autoFocus
									>
										Delete
									</Button>
								</DialogActions>
							</Dialog>
						</Grid>
					</Grid>
					<Grid
						item
						container
						direction="column"
						justify="flex-start"
						xs
					>
						<Grid item>
							<SubNotes
								noteId={id}
								subNotes={subNotes}
							/>
						</Grid>
						<Grid item>
							<CreateSubNote noteId={id}/>
						</Grid>
					</Grid>
				</Grid>
			</Card>
		</Box>
	)
};

export default Note;

Note.propTypes = {
	id: PropTypes.string,
	text: PropTypes.string,
	textAreaRows: PropTypes.number,
	subTasks: PropTypes.array
};
