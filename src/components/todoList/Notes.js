import React, {useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import Note from "./Note";
import {useNotes} from "../../store/todo/todo-selector";
import {fetchNotes} from "../../store/todo/todo-actions";
import Loader from "../common/Loader";
import {AuthorizationManager} from "../../utils/authorization-manager";
import Alert from "../common/Alert";


export const Notes = () => {

	const dispatch = useDispatch();

	const {notes = [], currentPage, total, error} = useNotes();
	const [showModal, setShowModal] = useState(false);
	const [isNeedMore, setMore] = useState(true);

	useEffect(() => {
		if (AuthorizationManager.getToken()) {
			dispatch(fetchNotes())
				.catch(() => setShowModal(true));
		}
	}, [dispatch]);

	useEffect(() => {
		setMore(notes.length < total);
	}, [notes, total]);

	const getNextPage = () => {
		dispatch(fetchNotes(currentPage + 1));
	};

	return (
		<>
			<InfiniteScroll
				next={getNextPage}
				hasMore={isNeedMore}
				loader={<Loader display="flex"/>}
				dataLength={notes.length}
			>{
				notes.map(note =>
					<Note key={note.created_at}
					      note={note}
					/>)
			}
			</InfiniteScroll>
			<Alert
				duration={3000}
				type='error'
				message={error?.message}
				open={showModal}
				onClose={() => setShowModal(false)}
			/>
		</>
	)
};
