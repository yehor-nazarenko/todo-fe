import React from 'react';
import {SubNote} from "./SubNote";


const SubNotes = (props) => {

    const {noteId, subNotes = []} = props;

    return (
        <>
            {
                subNotes.map(subNote =>
                    <SubNote
                        key={subNote.created_at}
                        noteId={noteId}
                        subNote={subNote}
                    />)
            }
        </>
    );
};

export default SubNotes;
