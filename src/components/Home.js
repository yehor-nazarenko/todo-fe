import React from 'react';
import {NavLink} from "react-router-dom";
import Grid from "@material-ui/core/Grid";

const Home = () => {
    return (
        <Grid
            container
        >
            <Grid
                item
                container
                direction="column"
                alignItems="center"
                spacing={3}
            >
                <Grid item>
                    Home Component will be implemented in the next version
                </Grid>
                <Grid item>
                    <NavLink to="/todos">
                        =====> go to TodoList
                    </NavLink>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Home;
