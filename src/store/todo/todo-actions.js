import {requestActionCreator, GET, POST, PUT, DELETE} from "../../utils/request-helper";
import {success} from "redux-saga-requests";
import {v4 as uuidv4} from 'uuid';


export const FETCH_NOTES = 'FETCH_NOTES';
export const fetchNotes = (page = 1) => requestActionCreator(FETCH_NOTES, `/notes?page=${page}`, GET);

export const CREATE_NOTE = 'CREATE_NOTE';
export const createNote = (content = '') => dispatch => {
	const payload = {data: {id: uuidv4(), content, children: [], created_at: Date.now(), local: true}};
	dispatch({type: success(CREATE_NOTE), payload})
	dispatch(requestActionCreator(CREATE_NOTE, `/notes`, POST, {content}, payload.data));
}

export const EDIT_NOTE = 'EDIT_NOTE';
export const editNote = (id, content = '') => (dispatch, getState) => {
	const notes = getState().todo.notes.map(note => note.id === id ? {...note, content} : note);
	dispatch({type: success(EDIT_NOTE), notes});
	dispatch(requestActionCreator(EDIT_NOTE, `/notes/${id}`, PUT, {content}, notes))
};

export const DELETE_NOTE = 'DELETE_NOTE';
export const deleteNote = (id) => (dispatch, getState) => {
	const notes = getState().todo.notes.filter(note => note.id !== id);
	dispatch({type: success(DELETE_NOTE), notes});
	dispatch(requestActionCreator(DELETE_NOTE, `/notes/${id}`, DELETE))
};

export const CREATE_SUB_NOTE = 'CREATE_SUB_NOTE';
export const createSubNote = (noteId, content) => dispatch => {
	const payload = {data: {id: uuidv4(), parent_id: noteId, content, created_at: Date.now(), local: true}};
	dispatch({type: success(CREATE_SUB_NOTE), payload});
	dispatch(requestActionCreator(CREATE_SUB_NOTE, `/notes/${noteId}/subNotes`, POST, {content}, payload.data));
}


export const EDIT_SUB_NOTE = 'EDIT_SUB_NOTE';
export const editSubNote = (noteId, subNoteId, content, completed) => (dispatch, getState) => {
	const notes = getState().todo.notes.map(note => note.id === noteId
		? {...note, children: note.children.map(subNote => subNote.id === subNoteId
				? {...subNote, content, completed}
				: subNote)}
		: note);
	dispatch({type: success(EDIT_SUB_NOTE), notes});
	dispatch(requestActionCreator(EDIT_SUB_NOTE, `/notes/${noteId}/subNotes/${subNoteId}`, PUT, {content, completed}))
};

export const DELETE_SUB_NOTE = 'DELETE_SUB_NOTE';
export const deleteSubNote = (noteId, subNoteId) => (dispatch, getState) => {
	const notes = getState().todo.notes.map(note => note.id === noteId
		? {...note, children: note.children.filter(subNote => subNote.id !== subNoteId)}
		: note);
	dispatch({type: success(DELETE_SUB_NOTE), notes});
	dispatch(requestActionCreator(DELETE_SUB_NOTE, `/notes/${noteId}/subNotes/${subNoteId}`, DELETE))
};

