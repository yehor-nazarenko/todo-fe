import {
	success,
	error
} from 'redux-saga-requests';
import {
	CREATE_SUB_NOTE,
	CREATE_NOTE,
	DELETE_SUB_NOTE,
	DELETE_NOTE,
	EDIT_SUB_NOTE,
	EDIT_NOTE,
	FETCH_NOTES
} from "./todo-actions";


const initialState = {
	notes: [],
	total: 0,
	currentPage: 0,
	error: null
};

export default (state = initialState, action) => {

	switch (action.type) {
		case CREATE_NOTE:
		case DELETE_NOTE:
		case FETCH_NOTES:
		case EDIT_NOTE:
		case CREATE_SUB_NOTE:
		case EDIT_SUB_NOTE:
		case DELETE_SUB_NOTE: {
			return {
				...state
			};
		}
		case success(FETCH_NOTES):
			const {data: newNotes, total, current_page: currentPage} = action.payload.data;
			return {
				...state,
				notes: currentPage === 1 ? newNotes : [...state.notes, ...newNotes],
				total,
				currentPage
			};
		case success(CREATE_NOTE): {
			const {data: newNote} = action.payload;
			const notes = newNote.local
				? [newNote, ...state.notes]
				: state.notes.map(note => note.id === action.meta.id
					? newNote
					: note);
			return {
				...state,
				notes
			};
		}
		case success(CREATE_SUB_NOTE): {
			const {data: newSubNote} = action.payload;
			const notes = newSubNote.local
				? state.notes.map(note => note.id === newSubNote.parent_id
					? {...note, children: [...note.children, newSubNote]}
					: note)
				: state.notes.map(note => note.id === newSubNote.parent_id
					? {...note, children: note.children.map(subNote => subNote.id === action.meta.id
						? newSubNote
						: subNote)}
					: note)
			return {
				...state,
				notes
			}
		}
		case success(EDIT_NOTE):
		case success(EDIT_SUB_NOTE):
		case success(DELETE_NOTE):
		case success(DELETE_SUB_NOTE): {
			return {
				...state,
				notes: action.notes ? action.notes : state.notes
			};
		}
		case error(FETCH_NOTES):
		case error(CREATE_NOTE):
		case error(DELETE_NOTE):
		case error(EDIT_NOTE):
		case error(CREATE_SUB_NOTE):
		case error(EDIT_SUB_NOTE):
		case error(DELETE_SUB_NOTE): {
			return {
				...state,
				error: action.payload
			};
		}
		default:
			return state;
	}
};
