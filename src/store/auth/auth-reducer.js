import {
    success,
    error
} from 'redux-saga-requests'
import {
    FETCH_USER,
    GET_USER,
    LOGIN,
    LOGOUT,
    REGISTER
} from "./auth-actions";


const initialState = {
    user: null,
    isLoading: false,
    error: null
};

export default (state = initialState, action) => {

    switch (action.type) {
        case LOGIN:
        case REGISTER:
        case LOGOUT:
        case FETCH_USER:{
            return {
                ...state,
                isLoading: true,
            }
        }
        case success(LOGIN):
        case success(REGISTER):
        case success(GET_USER):{
            return {
                ...state,
                user: action.payload.data,
                isLoading: false,
            };
        }

        // case success(FETCH_USER):{
        //     return {
        //         ...state,
        //         isLoading: false,
        //         user: action.payload.data
        //     }
        // }
        case success(LOGOUT): {
            return {
                ...state,
                isLoading: false,
            };
        }
        case error(LOGIN):
        case error(REGISTER):
        case error(LOGOUT):
        case error(FETCH_USER):{
            return {
                ...state,
                error: action.payload,
                isLoading: false,
            };
        }
        default:
            return state;
    }
};
