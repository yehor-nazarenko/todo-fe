import React from 'react';
import {
    BrowserRouter,
    Route,
    Switch
} from "react-router-dom";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import Todos from "./components/todoList/Todos";
import PrivateRoute from "./routes/PrivateRoute";
import withLayout from "./layouts/withLayout";
import Profile from "./components/Profile";
import Home from "./components/Home";


const AppRouter = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path={"/"} exact component={withLayout(Home)}/>
                <Route path={"/register"} component={withLayout(Register)}/>
                <Route path={"/login"} component={withLayout(Login)}/>
                <PrivateRoute path={"/todos"} exact component={withLayout(Todos)}/>
                <PrivateRoute path={"/profile"} exact component={withLayout(Profile)}/>
            </Switch>
        </BrowserRouter>
    );

};

export default AppRouter;
